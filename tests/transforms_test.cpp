#include <iostream>
#include <cppmath/transforms.h>
#include <cppmath/rotations.h>
#include <cppmisc/misc.h>
#include <cppmisc/traces.h>


void test_rotations()
{
    Vec3 r1 {-0.8, 2.5, -0.3};
    Mat3 R = rotmat(r1);
    Vec3 r2 = rodrigues(R);
    std::cout << r1 << std::endl << r2 << std::endl;
    assert(equal(r1, r2));
    unused(r1);
    unused(r2);
}

void test_transform()
{
    Transform T_wo(rotmat({0.2, 0.3, 0.4}), {2., 1., 3.}, "world", "object");
    Transform T_ow = inv(T_wo);
    Transform T_om(rotmat({0.1, 0.2, 0.3}), {2., 2., 1.}, "object", "marker");
    Transform T_mo = inv(T_om);
    Transform T_wm = T_wo * T_om;
    std::cout << T_wm << std::endl;
    std::cout << T_wm * T_mo * T_ow << std::endl;
}

void test_pack()
{
    auto tmp = randmat<6,6,double>();
    auto cov = tmp * tmp.t();
    auto v = covariance_pack(cov);
    assert(equal(covariance_unpack(v), cov));
}

void test_rodrigues()
{
    Mat3 R({
        0, 1, 0,
        1, 0, 0,
        0, 0,-1
    });
    auto r = rodrigues(R);
    auto R1 = rotmat(r);
    info_msg(R1);
}

int main(int argc, char const* argv[])
{
    test_pack();
    test_rotations();
    test_transform();
    test_rodrigues();
    return 0;
}
