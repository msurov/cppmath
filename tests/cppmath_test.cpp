#include <iostream>
#include <cppmisc/timing.h>
#include <cppmisc/misc.h>
#include <cppmath/math.h>
#include <cppmath/quat.h>
#include <cppmath/rotations.h>


void test_eigen()
{
    static_assert(isqrt(0) == 0, "isqrt works wrong");
    static_assert(isqrt(1) == 1, "isqrt works wrong");
    static_assert(isqrt(4) == 2, "isqrt works wrong");
    static_assert(isqrt(9) == 3, "isqrt works wrong");
    static_assert(isqrt(100) == 10, "isqrt works wrong");
    static_assert(isqrt(10000) == 100, "isqrt works wrong");
}

void test_exterior()
{
    Vec5 a {0.0488135039273248, 0.2151893663724195, 0.1027633760716439, 0.0448831829968969, -0.0763452006610953};
    Vec5 b {0.1458941130666561, -0.0624127887373075, 0.3917730007820798, 0.4636627605010293, -0.1165584811742223};
    Mat5 E {
        { 0.0000000000000000, -0.0344414486564237,  0.0041312413045881,  0.0160848118057305,  0.0054486874587855},
        { 0.0344414486564237,  0.0000000000000000,  0.0907191326808091,  0.1025765802609466, -0.0298470625891808},
        {-0.0041312413045881, -0.0907191326808091,  0.0000000000000000,  0.0300635313404383,  0.0179320453230611},
        {-0.0160848118057305, -0.1025765802609466, -0.0300635313404383,  0.0000000000000000,  0.0301669108491455},
        {-0.0054486874587855,  0.0298470625891808, -0.0179320453230611, -0.0301669108491455,  0.0000000000000000}
    };
    assert(equal(exterior(a,b), E));
}

void test_cross()
{
    Vec3 a {0.0488135039273248,  0.2151893663724195,  0.1027633760716439};
    Vec3 b {0.0448831829968969, -0.0763452006610953,  0.1458941130666561};
    Vec3 c {0.0392403523150811, -0.0025092554475503, -0.0133850604621824};
    assert(equal(cross(a, b), c));
    assert(equal(dot(c,a), 0));
    assert(equal(dot(c,b), 0));
    assert(equal(Vec3(wedge(a) * b), c));
}

void test_vec()
{
    Vec5 p;
    p.fill(-1);
    p.block<2>(2) = {3,4};
    assert(equal(p, {-1,-1,3,4,-1}));
}

void test_cwise()
{
    MatT<4,4,int>  m {
         1, 2, 3, 4,
         5, 6, 7, 8,
         9,10,11,12,
        13,14,15,16
    };
    auto f = [](int e) -> float { return e * 0.5f; };
    auto m2 = cwise_func(m, f);
    MatT<4,4,float>  m_ {
         1.f, 2.f, 3.f, 4.f,
         5.f, 6.f, 7.f, 8.f,
         9.f,10.f,11.f,12.f,
        13.f,14.f,15.f,16.f
    };
    assert(equal(m2, m_ / 2.f));
    unused(m2);
}

void test_mat_inv()
{
    {
        Mat3 A {
            1,2,3,
            3,2,4,
            1,2,1
        };
        Mat3 Ainv {
            -0.75,   0.5 ,   0.25 ,
            0.125, -0.25,   0.625,
            0.5 ,  -0.0 ,  -0.5
        };
        assert(
            equal(inverse(A), Ainv)
        );
    }

    {
        Mat5 A {
            1.0, 2.0, 3.0, 4.0, 1.0,
            3.0, 2.0, 4.0, 6.0, 5.0,
            1.0, 2.0, 1.0, 2.0, 4.0,
            3.0, 7.0, 5.0, 2.0, 1.0,
            3.0, 2.0, 5.0, 6.0, 1.0
        };
        Mat5 Ainv {
            -1.48, -1.56,  1.88, -0.24,  2.0,
            -0.24, -1.28,  1.44, -0.12,  1.0,
             1.28,  3.16, -3.68,  0.64, -3.0,
            -0.30, -1.60,  1.80, -0.40,  1.5,
             0.32,  1.04, -0.92,  0.16, -1.0
        };
        assert(
            equal(inverse(A), Ainv)
        );
    }

}

void test_quat()
{
    QuatT<int> q1(1,2,3,4);
    QuatT<int> q2(-1,2,-3,4);
    assert(equal(q1 * q2, {-12,24,-6,-12}));
    assert(equal(q1.conj() * q2, {10, -20, 0, 20}));
    assert(equal(q1 + q2, {0, 4, 0, 8}));
}

void test_assign()
{
    Vec3 v{1,2,3};
    v = 0;
    Mat3 m{1,2,3,4,5,6,7,8,9};
    m = 0.0;
    assert(equal(v, Vec3::zeros()));
    assert(equal(m, Mat3::zeros()));
}

void test_vec_to_tuple()
{
    Vec4 v {5,3,2,4};
    double x,y,z,w;
    std::tie(x,y,z,w) = v.to_tuple();
    assert(x == v(0));
    assert(y == v(1));
    assert(z == v(2));
    assert(w == v(3));
}

int main(int argc, char const *argv[])
{
    test_vec_to_tuple();
	test_eigen();
    test_exterior();
    test_cross();
    test_vec();
    test_mat_inv();
    test_quat();
    test_cwise();
    test_assign();
	return 0;
}
