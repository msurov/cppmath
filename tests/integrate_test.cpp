#include <iostream>
#include <cppmath/math.h>
#include <cppmath/integrate.h>


void test_expm()
{
    auto f = [](double t, Mat2 const& x) {
        return Mat2{0, -1, 1, 0} * x;
    };
    auto sol = ode::solve(f, 0., M_PI, Mat2::eye(), 1e-3);
    auto X = *sol.x.rbegin();
    assert(equal(X, -Mat2::eye()));
    (void)X;
}

int main(int argc, char const* argv[])
{
    test_expm();
    return 0;
}
