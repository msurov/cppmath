#include <cmath>
#include <assert.h>
#include <cppmath/fminbound.h>
#include <cppmisc/misc.h>


void test()
{
    auto f = [](double x) -> double {
        return sin(x);
    };

    auto r = fminbound(f, -5, -1, 1e-8);
    assert(r.success);
    assert(fabs(r.x + M_PI_2) < 1e-8);
    unused(r);
}

int main(int argc, char const *argv[])
{
	test();
	return 0;
}
