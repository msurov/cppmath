#include <cppmath/rotations.h>


Vec3 rodrigues_to_rpy(Vec3 const& rodrigues)
{
    double theta = norm(rodrigues);
    if (fabs(theta) < 1e-15)
        return Vec3::zeros();
    Vec3 l = rodrigues / theta;
    double w,x,y,z;
    w = cos(theta/2);
    std::tie(x,y,z) = (l * sin(theta/2)).to_tuple();
    double roll = atan2(2 * w * x + 2 * y * z, 1 - 2 * (x * x + y * y));
    double pitch = asin(2 * w * y - 2 * z * x);
    double yaw = atan2(2 * w * z + 2 * y * x, 1 - 2 * (y * y + z * z));
    return {roll, pitch, yaw};
}
