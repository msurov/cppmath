#include <cppmath/transforms.h>


Transform inv(Transform const& T)
{
    auto inv_R = T.R.transpose();
    auto t = T.t;
    return {
        inv_R,
        -inv_R * t,
        T.child_frame,
        T.base_frame
    };
}

Pose transform_left(Transform const& T, Pose const& pose)
{
    if (T.child_frame && pose.base_frame)
        assert(!strcmp(T.child_frame, pose.base_frame));

    auto R1 = rotmat(pose.r);
    auto p1 = pose.p;

    auto R2 = T.R * R1;
    auto p2 = T.R * p1 + T.t;
    auto r2 = rodrigues(R2);

    return {r2, p2, T.base_frame, pose.child_frame};
}

Pose transform_right(Pose const& pose, Transform const& T)
{
    if (T.child_frame && pose.base_frame)
        assert(!strcmp(pose.child_frame, T.base_frame));

    auto R1 = rotmat(pose.r);
    auto p1 = pose.p;

    auto R2 = R1 * T.R;
    auto p2 = R1 * T.t + p1;
    auto r2 = rodrigues(R2);

    return {r2, p2, pose.base_frame, T.child_frame};
}

PoseCov transform_left(Transform const& T, PoseCov const& pose)
{
    if (T.child_frame && pose.base_frame)
        assert(!strcmp(T.child_frame, pose.base_frame));

    auto R1 = rotmat(pose.r);
    auto const& p1 = pose.p;
    auto const& cov1 = pose.cov;

    auto const& TR = T.R;
    auto const& Tt = T.t;

    auto R2 = TR * R1;
    auto p2 = TR * p1 + Tt;
    auto r2 = rodrigues(R2);

    auto Jac = Mat6::eye();
    Jac.copy(TR, 3, 3);

    auto cov2 = Jac * cov1 * Jac.transpose();

    return {
        r2, p2, cov2,
        T.base_frame, pose.child_frame
    };
}

PoseCov transform_right(PoseCov const& pose_AB, Transform const& T_BC)
{
    if (pose_AB.child_frame && T_BC.base_frame)
        assert(!strcmp(pose_AB.child_frame, T_BC.base_frame));

    auto R1 = rotmat(pose_AB.r);
    auto const& p1 = pose_AB.p;
    auto const& cov1 = pose_AB.cov;

    auto const& TR = T_BC.R;
    auto const& Tt = T_BC.t;

    auto R2 = R1 * TR;
    auto p2 = R1 * Tt + p1;
    auto r2 = rodrigues(R2);

    auto Jac = Mat6::eye();
    Jac.copy(TR.transpose(), 0, 0);
    Jac.copy(-R1 * wedge(Tt), 3, 0);
    auto cov2 = Jac * cov1 * Jac.transpose();

    return {
        r2, p2, cov2, 
        pose_AB.base_frame, T_BC.child_frame
    };
}

Pose relative_pose(Pose const& base, Pose const& child)
{
    if (base.base_frame && child.base_frame)
        assert(!strcmp(base.base_frame, child.base_frame));

    auto Ra = rotmat(base.r);
    auto const& ta = base.p;
    auto Rb = rotmat(child.r);
    auto const& tb = child.p;

    auto Rc = Ra.t() * Rb;
    auto rc = rodrigues(Rc);
    auto tc = Ra.t() * (tb - ta);

    return {
        rc, tc, 
        base.child_frame, child.child_frame
    };
}

PoseCov relative_pose(PoseCov const& base, PoseCov const& child)
{
    if (base.base_frame && child.base_frame)
        assert(!strcmp(base.base_frame, child.base_frame));

    auto Ra = rotmat(base.r);
    auto const& ta = base.p;
    auto const& cov_a = base.cov;
    auto Rb = rotmat(child.r);
    auto const& tb = child.p;
    auto const& cov_b = child.cov;

    auto Rc = Ra.t() * Rb;
    auto rc = rodrigues(Rc);
    auto tc = Ra.t() * (tb - ta);

    auto Jac = MatT<6,12>::zeros();
    auto I = MatT<3,3>::eye();
    Jac.copy(-Rc.t(), 0, 0);
    Jac.copy(I, 0, 6);
    Jac.copy(Ra.t() * wedge(tb - ta) * Ra, 3, 0);
    Jac.copy(-Ra.t(), 3, 3);
    Jac.copy(Ra.t(), 3, 9);

    auto cov_ab = MatT<12,12>::zeros();
    cov_ab.copy(cov_a, 0, 0);
    cov_ab.copy(cov_b, 6, 6);

    auto cov_c = Jac * cov_ab * Jac.t();

    return {
        rc, tc, cov_c, 
        base.child_frame, child.child_frame
    };
}

PoseCov inv(PoseCov const& pose)
{
    auto R1 = rotmat(pose.r);
    auto const& t1 = pose.p;
    auto const& cov1 = pose.cov;

    auto R2 = R1.t();
    auto r2 = rodrigues(R2);
    auto t2 = -R2 * t1;
    auto Jac = Mat6::zeros();
    Jac.copy(-R1, 0, 0);
    Jac.copy(wedge(t2), 3, 0);
    Jac.copy(-R2, 3, 3);
    auto cov2 = Jac * cov1 * Jac.t();
    
    return {
        r2, t2, cov2, 
        pose.child_frame, pose.base_frame
    };
}

Pose inv(Pose const& pose)
{
    auto R1 = rotmat(pose.r);
    auto const& t1 = pose.p;

    auto R2 = R1.t();
    auto r2 = rodrigues(R2);
    auto t2 = -R2 * t1;
  
    return {
        r2, t2,
        pose.child_frame, pose.base_frame
    };
}

Transform transform(Pose const& pose)
{
    auto R = rotmat(pose.r);
    auto const& t = pose.p;
    return {
        R, t,
        pose.base_frame, pose.child_frame
    };
}

Transform transform(Vec3 const& r, Vec3 const& p,
    char const* base_frame, char const* child_frame)
{
    auto R = rotmat(r);
    auto const& t = p;
    return {
        R, t,
        base_frame, child_frame
    };
}

VecT<21> covariance_pack(Mat6 const& cov_rp)
{
    double rx_rx = cov_rp(0,0);
    double rx_ry = cov_rp(0,1);
    double rx_rz = cov_rp(0,2);
    double rx_px = cov_rp(0,3);
    double rx_py = cov_rp(0,4);
    double rx_pz = cov_rp(0,5);

    double ry_ry = cov_rp(1,1);
    double ry_rz = cov_rp(1,2);
    double ry_px = cov_rp(1,3);
    double ry_py = cov_rp(1,4);
    double ry_pz = cov_rp(1,5);

    double rz_rz = cov_rp(2,2);
    double rz_px = cov_rp(2,3);
    double rz_py = cov_rp(2,4);
    double rz_pz = cov_rp(2,5);

    double px_px = cov_rp(3,3);
    double px_py = cov_rp(3,4);
    double px_pz = cov_rp(3,5);

    double py_py = cov_rp(4,4);
    double py_pz = cov_rp(4,5);

    double pz_pz = cov_rp(5,5);

    double px_rx = rx_px; 
    double px_ry = ry_px;
    double px_rz = rz_px;
    double py_rx = rx_py;
    double py_ry = ry_py;
    double py_rz = rz_py;
    double pz_rx = rx_pz;
    double pz_ry = ry_pz;
    double pz_rz = rz_pz;

    return {
        px_px, px_py, px_pz, px_rx, px_ry, px_rz,
               py_py, py_pz, py_rx, py_ry, py_rz,
                      pz_pz, pz_rx, pz_ry, pz_rz,
                             rx_rx, rx_ry, rx_rz,
                                    ry_ry, ry_rz,
                                           rz_rz,
    };
}

Mat6 covariance_unpack(VecT<21> const& packed)
{
    auto [
        px_px, px_py, px_pz, px_rx, px_ry, px_rz,
               py_py, py_pz, py_rx, py_ry, py_rz,
                      pz_pz, pz_rx, pz_ry, pz_rz,
                             rx_rx, rx_ry, rx_rz,
                                    ry_ry, ry_rz,
                                           rz_rz] = packed.to_tuple();

    return {
        rx_rx, rx_ry, rx_rz, px_rx, py_rx, pz_rx,
        rx_ry, ry_ry, ry_rz, px_ry, py_ry, pz_ry,
        rx_rz, ry_rz, rz_rz, px_rz, py_rz, pz_rz,
        px_rx, px_ry, px_rz, px_px, px_py, px_pz,
        py_rx, py_ry, py_rz, px_py, py_py, py_pz,
        pz_rx, pz_ry, pz_rz, px_pz, py_pz, pz_pz,
    };
}

std::ostream& operator << (std::ostream& s, Transform const& T)
{
    auto r = rodrigues(T.R);
    auto t = T.t;
    char buf[256];
    int n = snprintf(
        buf, sizeof(buf), "{r=(%f, %f, %f), t=(%f, %f, %f)}", 
        r(0), r(1), r(2), 
        t(0), t(1), t(2)
    );
    if (n > 0)
    {
        char const* to = T.base_frame ? T.base_frame : "undef";
        char const* from = T.child_frame ? T.child_frame : "undef";
        s << "T[" << to << " <- " << from << "] = " << buf;
    }
    return s;
}

std::ostream& operator << (std::ostream& s, Pose const& pose)
{
    auto r = pose.r;
    auto p = pose.p;
    char buf[256];
    int n = snprintf(
        buf, sizeof(buf), "{r=(%f, %f, %f), p=(%f, %f, %f)}", 
        r(0), r(1), r(2), 
        p(0), p(1), p(2)
    );
    if (n > 0)
    {
        char const* to = pose.base_frame ? pose.base_frame : "undef";
        char const* obj = pose.child_frame ? pose.child_frame : "undef";
        s << "pose[" << to << ", " << obj << "] = " << buf;
    }
    return s;
}
