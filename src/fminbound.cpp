#include <cppmath/fminbound.h>
#include <cppmath/mat.h>

#include <assert.h>
#include <math.h>
#include <tuple>


OptimizeResult fminbound(
    std::function<double(double)> func,
    double x1, double x2, double xatol, int maxiter)
{
    int maxfun = maxiter;
    assert(x2 > x1);

    double sqrt_eps = sqrt(2.2e-16);
    double golden_mean = 0.5 * (3.0 - sqrt(5.0));

    double a = x1;
    double b = x2;
    double fulc = a + golden_mean * (b - a);
    double nfc = fulc, xf = fulc;
    double rat = 0.0, e = 0.0;

    double x = xf;
    double fx = func(x);
    int num = 1;
    auto fmin_data = std::make_tuple(1, xf, fx);

    double ffulc = fx, fnfc = fx;
    double xm = 0.5 * (a + b);
    double tol1 = sqrt_eps * fabs(xf) + xatol / 3.0;
    double tol2 = 2.0 * tol1;
    int flag = 0;

    while (fabs(xf - xm) > (tol2 - 0.5 * (b - a)))
    {
        int golden = 1;
        // Check for parabolic fit
        if (fabs(e) > tol1)
        {
            golden = 0;
            double r = (xf - nfc) * (fx - ffulc);
            double q = (xf - fulc) * (fx - fnfc);
            double p = (xf - fulc) * q - (xf - nfc) * r;
            q = 2.0 * (q - r);

            if (q > 0.0)
            {
                p = -p;
            }
            q = fabs(q);
            r = e;
            e = rat;

            // Check for acceptability of parabola
            if ((fabs(p) < fabs(0.5*q*r)) && (p > q*(a - xf)) && (p < q * (b - xf)))
            {
                rat = (p + 0.0) / q;
                x = xf + rat;

                if (((x - a) < tol2) || ((b - x) < tol2))
                {
                    double si = xm >= xf ? 1. : -1;
                    rat = tol1 * si;
                }
            }
            else
            {
                golden = 1;
            }
        }

        if (golden)
        {
            if (xf >= xm)
                e = a - xf;
            else
                e = b - xf;
            rat = golden_mean * e;
        }

        double si = rat >= 0. ? 1. : -1.;
        x = xf + si * std::max(fabs(rat), tol1);
        double fu = func(x);
        num += 1;
        fmin_data = std::make_tuple(num, x, fu);

        if (fu <= fx)
        {
            if (x >= xf)
                a = xf;
            else
                b = xf;
            fulc = nfc;
            ffulc = fnfc;
            nfc = xf;
            fnfc = fx;
            xf = x;
            fx = fu;
        }
        else
        {
            if (x < xf)
                a = x;
            else
                b = x;

            if ((fu <= fnfc) || equal(nfc, xf))
            {
                fulc = nfc;
                ffulc = fnfc;
                nfc = x;
                fnfc = fu;
            }
            else if ((fu <= ffulc) || equal(fulc, xf) || equal(fulc, nfc))
            {
                fulc = x;
                ffulc = fu;
            }
        }

        xm = 0.5 * (a + b);
        tol1 = sqrt_eps * fabs(xf) + xatol / 3.0;
        tol2 = 2.0 * tol1;

        if (num >= maxfun)
        {
            flag = 1;
            break;
        }
    }

    OptimizeResult result;
    result.x = xf;
    result.fun = fx;
    result.nfev = num;
    result.status = flag;
    result.success = flag == 0;

    return result;
}
