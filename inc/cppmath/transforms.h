#pragma once

#include <cstring>
#include <assert.h>
#include "rotations.h"


struct Pose
{
    char const* base_frame;
    char const* child_frame;
    Vec3 r;
    Vec3 p;

    Pose() : base_frame(nullptr), child_frame(nullptr), r(Vec3::zeros()), p(Vec3::zeros()) {}

    Pose(Vec3 const& r, Vec3 const& p, 
        char const* base_frame = nullptr, char const* child_frame = nullptr) : 
        base_frame(base_frame), child_frame(child_frame), r(r), p(p) {}
    
    inline Vec3 rpy() const
    {
        return rodrigues_to_rpy(r);
    }
};

struct PoseCov : public Pose
{
    Mat6 cov; // in variables (r,p)

    PoseCov() : Pose(), cov(Mat6::zeros()) {}

    PoseCov(Vec3 const& r, Vec3 const& p, Mat6 const& cov, 
        char const* base_frame = nullptr, char const* child_frame = nullptr) :
        Pose(r, p, base_frame, child_frame), cov(cov) {}
};

struct Transform
{
    char const* base_frame;
    char const* child_frame;
    Mat3 R;
    Vec3 t;

    Transform() : base_frame(nullptr), child_frame(nullptr), 
        R(Mat3::eye()), t(Vec3::zeros()) {}

    Transform(Mat3 const& R, Vec3 const& t, 
        char const* base_frame = nullptr, char const* child_frame = nullptr) : 
        base_frame(base_frame), child_frame(child_frame), R(R), t(t) {}
};

/**
 * @brief composition of two transforms
 * @param T_AB transform A <- B
 * @param T_BC transform B <- C
 * @result T_AC transform A <- C
 */
inline Transform operator * (Transform const& T_AB, Transform const& T_BC)
{
    if (T_AB.child_frame && T_BC.base_frame)
        assert(!strcmp(T_AB.child_frame, T_BC.base_frame));

    return {
        T_AB.R * T_BC.R,
        T_AB.R * T_BC.t + T_AB.t,
        T_AB.base_frame,
        T_BC.child_frame
    };
}

/**
 * @brief packs covariance matrix into array of its components
 * @param cov_rp real-valued symmetric covariance matrix 6x6 
 *  define covariance of the pose in variables (rx,ry,rz,px,py,pz)
 * @result array of components of the covariance matrix
 *  in the following order: (px_px, px_py, px_pz, px_rx, px_ry, px_rz, py_py, py_pz, ...)
 */
VecT<21> covariance_pack(Mat6 const& cov_rp);
/**
 * @brief unpacks covariance matrix from array of its components
 * @param packed array of components of the covariance matrix
 *  packed in the following order: (px_px, px_py, px_pz, px_rx, px_ry, px_rz, py_py, py_pz, ...)
 * @result cov_rp covariance matrix 6x6 
 *  defines covariance of the pose in variables (rx,ry,rz,px,py,pz)
 */
Mat6 covariance_unpack(VecT<21> const& packed);

/**
 * @param T transform B -> A
 * @result transform A -> B
 */
Transform inv(Transform const& T);
/**
 * @param pose pose of frame C wrt B
 * @param T transform B -> A
 * @result pose of frame C wrt A
 */
Pose transform_left(Transform const& T, Pose const& pose);
/**
 * @param pose pose of frame B wrt A
 * @param T transform C -> B
 * @result pose of frame C wrt A
 */
Pose transform_right(Pose const& pose, Transform const& T);
/**
 * @param pose pose of frame C wrt B
 * @param T transform B -> A
 * @result pose of frame C wrt A
 */
PoseCov transform_left(Transform const& T, PoseCov const& pose);
/**
 * @param pose_AB pose of frame B wrt A
 * @param T_BC transform C -> B
 * @result pose_AC of frame C wrt A
 */
PoseCov transform_right(PoseCov const& pose_AB, Transform const& T_BC);
/**
 * @param pose_AB pose of frame B wrt A
 * @param pose_AC pose of frame C wrt A
 * @result pose_BC pose of frame C wrt B
 */
PoseCov relative_pose(PoseCov const& pose_AB, PoseCov const& pose_AC);
/**
 * @param base pose of frame B wrt A
 * @param child pose of frame C wrt A
 * @result pose of frame C wrt B
 */
Pose relative_pose(Pose const& base, Pose const& child);
/**
 * @param pose pose of frame B wrt A
 * @result pose of frame A wrt B
 */
Pose inv(Pose const& pose);
/**
 * @param pose pose of frame B wrt A
 * @result pose of frame A wrt B
 */
PoseCov inv(PoseCov const& pose);
/**
 * @param pose of frame B wrt A
 * @result transform B -> A
 */
Transform transform(Pose const& pose);
/**
 * @param r rodrigues
 * @param p position
 * @param base_frame
 * @param child_frame
 * @result transform child_frame -> base_frame
 */
Transform transform(
    Vec3 const& r,
    Vec3 const& p,
    char const* base_frame = nullptr,
    char const* child_frame = nullptr
);

/**
 * @brief format transform and pose to ostream
 */
std::ostream& operator << (std::ostream& s, Transform const& T);
std::ostream& operator << (std::ostream& s, Pose const& T);
