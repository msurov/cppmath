#pragma once

#include <cmath>
#include <memory>
#include <ostream>
#include <initializer_list>
#include <vector>
#include <assert.h>
#include <tuple>


template <int N, typename T=double>
struct VecT;

using Vec2 =  VecT<2>;
using Vec3 =  VecT<3>;
using Vec4 =  VecT<4>;
using Vec5 =  VecT<5>;
using Vec6 =  VecT<6>;
using Vec10 = VecT<10>;
using Vec11 = VecT<11>;
using Vec12 = VecT<12>;
using Vec13 = VecT<13>;

template <int N, int M, typename T=double>
struct MatT;

using Mat2x5 =  MatT<2,5>;
using Mat5x2 =  MatT<5,2>;
using Mat3x2 =  MatT<3,2>;
using Mat2x10 = MatT<2,10>;

using Mat2x2 = MatT<2,2>;
using Mat2 = MatT<2,2>;
using Mat2x1 = MatT<2,1>;
using Mat1x2 = MatT<1,2>;
using Mat3 = MatT<3,3>;
using Mat4 = MatT<4,4>;
using Mat5 = MatT<5,5>;
using Mat6 = MatT<6,6>;


template <int N, typename T>
struct VecT
{
protected:
    static_assert(N > 0, "Expect positive dimension of vector");
    T _data[N];

    template <std::size_t... Indices>
    inline auto __to_tuple(std::index_sequence<Indices...>) const
    {
        return std::forward_as_tuple(_data[Indices]...);
    }

public:
    using value_type = T;

    VecT()
    {
    }

    explicit VecT(T const* pmem)
    {
        for (int i = 0; i < N; ++ i)
            _data[i] = pmem[i];
    }

    VecT(std::initializer_list<T> const& list)
    {
        assert(list.size() == N);

        auto p = list.begin();
        for (int i = 0; i < N; ++ i)
        {
            _data[i] = *p;
            ++ p;
        }
    }

    inline auto to_tuple() const
    {
        return __to_tuple(std::make_index_sequence<N>{});
    }

    static VecT zeros()
    {
        VecT result;
        result.fill(0);
        return result;
    }

    inline void fill(T val)
    {
        for (int i = 0; i < N; ++ i)
            _data[i] = val;
    }

    inline T& at(int i)
    {
        return _data[i];
    }

    inline T at(int i) const
    {
        return _data[i];
    }

    template <int K>
    inline VecT<K> const& block(int start) const
    {
        assert(start + K <= N);
        VecT<K> const* p = reinterpret_cast<VecT<K> const*>(
            _data + start
        );
        return *p;
    }

    template <int K>
    inline VecT<K>& block(int start)
    {
        assert(start + K <= N);
        VecT<K>* p = reinterpret_cast<VecT<K>*>(
            _data + start
        );
        return *p;
    }

    inline T const* data() const
    {
        return _data;
    }

    inline T* data()
    {
        return _data;
    }

    inline VecT mul(T k) const
    {
        VecT a;
        for (int i = 0; i < N; ++ i)
            a._data[i] = k * _data[i];
        return a;
    }

    inline VecT div(T k) const
    {
        VecT a;
        for (int i = 0; i < N; ++ i)
            a._data[i] = _data[i] / k;
        return a;
    }

    inline VecT add(VecT a) const
    {
        VecT b;
        for (int i = 0; i < N; ++ i)
            b._data[i] = _data[i] + a._data[i];
        return b;
    }

    inline VecT sub(VecT a) const
    {
        VecT b;
        for (int i = 0; i < N; ++ i)
            b._data[i] = _data[i] - a._data[i];
        return b;
    }

    inline T dot(VecT const& b) const
    {
        T val = at(0) * b(0);
        for (int i = 1; i < N; ++ i)
            val += at(i) * b(i);
        return val;
    }

    inline T norm() const
    {
        return std::sqrt(dot(*this));
    }

    inline VecT normalized() const
    {
        T n = norm();
        return div(n);
    }

    inline T& operator () (int i)
    {
        return at(i);
    }

    inline T operator () (int i) const
    {
        return at(i);
    }

    inline VecT& operator += (VecT const& v)
    {
        for (int i = 0; i < N; ++ i)
            _data[i] += v._data[i];
        return *this;
    }

    inline VecT& operator -= (VecT const& v)
    {
        for (int i = 0; i < N; ++ i)
            _data[i] -= v._data[i];
        return *this;
    }

    inline VecT& operator *= (T k)
    {
        for (int i = 0; i < N; ++ i)
            _data[i] *= k;
        return *this;
    }

    inline VecT& operator /= (T k)
    {
        for (int i = 0; i < N; ++ i)
            _data[i] /= k;
        return *this;
    }

    constexpr int size() const
    {
        return N;
    }

    inline VecT const& operator = (T const& val)
    {
        this->fill(val);
        return *this;
    }
};

template <int N, typename T>
inline VecT<N,T> operator + (VecT<N,T> const& a, VecT<N,T> const& b)
{
    return a.add(b);
}

template <int N, typename T>
inline VecT<N,T> operator - (VecT<N,T> const& a, VecT<N,T> const& b)
{
    return a.sub(b);
}

template <int N, typename T>
inline VecT<N,T> operator * (VecT<N,T> const& a, T k)
{
    return a.mul(k);
}

template <int N, typename T>
inline T operator * (VecT<N,T> const& a, VecT<N,T> const& b)
{
    return a.dot(b);
}

template <int N, typename T>
inline VecT<N,T> operator * (T k, VecT<N,T> const& a)
{
    return a.mul(k);
}

template <int N, typename T>
inline VecT<N,T> operator / (VecT<N,T> const& a, T k)
{
    return a.div(k);
}


template <int N, int M, typename T>
struct MatT
{
protected:
    static_assert(N > 0, "dimension N must be positive");
    static_assert(M > 0, "dimension M must be positive");

    T _data[N*M];

    template <class ... Tail>
    inline void __fill_diag(T first, Tail ... tail)
    {
        constexpr int i = N - sizeof...(tail) - 1;
        static_assert(i <= N, "incorrect number of arguments");
        static_assert(M == N, "works for square matrix only");
        at(i,i) = first;
        __fill_diag(tail...);
    }

    inline void __fill_diag()
    {
    }

public:
    using value_type = T;

    MatT()
    {
    }

    MatT(std::initializer_list<T> const& list)
    {
        assert(list.size() == N * M);

        auto p = list.begin();
        for (int i = 0; i < N*M; ++ i)
        {
            _data[i] = *p;
            ++ p;
        }
    }

    MatT(std::initializer_list<std::initializer_list<T>> const& list)
    {
        assert(list.size() == N);
        assert(list.begin()->size() == M);

        auto rows = list.begin();

        for (int y = 0; y < N; ++ y)
        {
            T* row = row_ptr(y);
            auto elems = rows->begin();

            for (int x = 0; x < M; ++ x)
            {
                row[x] = *elems;
                ++ elems;
            }

            ++ rows;
        }
    }

    inline void fill(T val)
    {
        for (int i = 0; i < N*M; ++ i)
        {
            _data[i] = val;
        }
    }

    inline T* row_ptr(int y) { return &_data[y * M]; }
    inline T const* row_ptr(int y) const { return &_data[y * M]; }
    inline T* data() { return _data; }
    inline T const* data() const { return _data; }

    inline T& at(int y, int x)
    {
        return _data[y*M + x];
    }

    inline T const& at(int y, int x) const
    {
        return _data[y*M + x];
    }

    inline T& operator () (int y, int x)
    {
        return at(y, x);
    }

    inline T const& operator () (int y, int x) const
    {
        return at(y,x);
    }

    template <int C, int D>
    MatT<C,D> block(int y_start, int x_start)
    {
        assert(C + y_start <= N);
        assert(D + x_start <= M);

        MatT<C,D> c;

        for (int y = 0; y < C; ++ y)
        {
            T const* srow = this->row_ptr(y + y_start);
            T* drow = c.row_ptr(y);

            for (int x = 0; x < D; ++ x)
                drow[x] = srow[x + x_start];
        }
    }

    template <int C, int D>
    void copy(MatT<C,D> const& src, int y_start, int x_start)
    {
        assert(y_start + C <= N);
        assert(x_start + D <= M);

        for (int y = 0; y < C; ++y)
        {
            T const* sptr = &src.at(y, 0);
            T* dptr = &at(y + y_start, x_start);

            for (int x = 0; x < D; ++x)
            {
                dptr[x] = sptr[x];
            }
        }
    }

    inline MatT mul(T k) const
    {
        MatT c;

        for (int y = 0; y < N; ++ y)
        {
            T const* srow = this->row_ptr(y);
            T* drow = c.row_ptr(y);

            for (int x = 0; x < M; ++ x)
                drow[x] = srow[x] * k;
        }

        return c;
    }

    inline MatT div(T k) const
    {
        MatT c;

        for (int y = 0; y < N; ++ y)
        {
            T const* srow = this->row_ptr(y);
            T* drow = c.row_ptr(y);

            for (int x = 0; x < M; ++ x)
                drow[x] = srow[x] / k;
        }

        return c;
    }

    inline MatT add(MatT b) const
    {
        MatT c;

        for (int y = 0; y < N; ++ y)
        {
            T const* srow1 = this->row_ptr(y);
            T const* srow2 = b.row_ptr(y);
            T* drow = c.row_ptr(y);
            for (int x = 0; x < M; ++ x)
               drow[x] = srow1[x] + srow2[x];

        }

        return c;
    }

    inline MatT sub(MatT b) const
    {
        MatT c;

        for (int y = 0; y < N; ++ y)
        {
            T const* srow1 = this->row_ptr(y);
            T const* srow2 = b.row_ptr(y);
            T* drow = c.row_ptr(y);

            for (int x = 0; x < M; ++ x)
                drow[x] = srow1[x] - srow2[x];
        }

        return c;
    }

    static MatT eye()
    {
        static_assert(N == M, "eye is defined only for square matrices");

        MatT I;
        I.fill(0);
        for (int i = 0; i < N; ++ i)
            I(i,i) = T(1);

        return I;
    }

    inline double trace() const
    {
        static_assert(N == M, "trace is defined only for square matrices");
        double tr = 0.;
        for (int i = 0; i < N; ++ i)
            tr += at(i,i);
        return tr;
    }

    static MatT zeros()
    {
        MatT m;
        m.fill(0);
        return m;
    }

    template <class ... Args>
    static MatT diag(Args... args)
    {
        static_assert(N == M, "works for square matrix only");
        static_assert(sizeof...(args) == N, "incorrect number of arguments");
        MatT D;
        D.fill(0);
        D.__fill_diag(args...);
        return D;
    }

    VecT<N,T> diag() const
    {
        static_assert(N == M, "works for square matrix only");
        VecT<N,T> d;
        for (int i = 0; i < N; ++ i)
            d(i) = at(i,i);
        return d;
    }

    inline VecT<N> get_col(int x) const
    {
        VecT<N> c;
        for (int y = 0; y < N; ++ y)
            c(y) = at(y,x);
        return c;
    }

    inline void set_col(int x, VecT<N> const& v)
    {
        for (int y = 0; y < N; ++ y)
            at(y,x) = v(y);
    }

    inline void swap_cols(int i, int j)
    {
        for (int y = 0; y < N; ++ y)
        {
            std::swap(at(y, i), at(y, j));
        }
    }

    inline void swap_rows(int i, int j)
    {
        T* r1 = row_ptr(i);
        T* r2 = row_ptr(j);

        for (int x = 0; x < M; ++ x)
        {
            std::swap(r1[x], r2[x]);
        }
    }

    inline MatT<M,N> transpose() const
    {
        MatT<M,N> C;

        for (int y = 0; y < N; ++ y)
        {
            T const* srow = row_ptr(y);

            for (int x = 0; x < M; ++ x)
            {
                C(x,y) = srow[x];
            }
        }

        return C;
    }

    inline MatT<M,N> t() const
    {
        return transpose();
    }

    constexpr int nrows() const
    {
        return N;
    }

    constexpr int ncols() const
    {
        return M;
    }

    constexpr int total() const
    {
        return M * N;
    }

    inline MatT& operator += (MatT const& v)
    {
        const int n = total();

        for (int i = 0; i < n; ++ i)
            _data[i] += v._data[i];
        return *this;
    }

    inline MatT& operator -= (MatT const& v)
    {
        constexpr int n = total();
        for (int i = 0; i < n; ++ i)
            _data[i] -= v._data[i];
        return *this;
    }

    inline MatT const& operator = (T const& val)
    {
        this->fill(val);
        return *this;
    }
};


template <int N, int M, typename T>
inline MatT<N,M,T> operator + (MatT<N,M,T> const& A, MatT<N,M,T> const& B) { return A.add(B); }

template <int N, int M, typename T>
inline MatT<N,M,T> operator - (MatT<N,M,T> const& A, MatT<N,M,T> const& B) { return A.sub(B); }

template <int N, int M, typename T>
inline MatT<N,M,T> operator * (MatT<N,M,T> const& m, T k) { return m.mul(k); }

template <int N, int M, typename T>
inline MatT<N,M,T> operator * (T k, MatT<N,M,T> const& m) { return m.mul(k); }

template <int N, int M, typename T>
inline MatT<N,M,T> operator / (MatT<N,M,T> const& m, T k) { return m.div(k); }


template <typename T>
inline void __fill_rand(T* pmem, int N, T vmin, T vmax)
{
    assert(vmax > vmin);
    for (int i = 0; i < N; ++ i)
    {
        pmem[i] = vmin + rand() * (vmax - vmin) / RAND_MAX;
    }
}

template <int N, int M, typename T>
inline MatT<N,M,T> randmat(T vmin = 0, T vmax = 1)
{
    MatT<N,M,T> A;
    __fill_rand(A.data(), N*M, vmin, vmax);
    return A;
}

template <int N, typename T>
inline VecT<N,T> randvec(T vmin = 0, T vmax = 1)
{
    VecT<N,T> v;
    __fill_rand(v.data(), N, vmin, vmax);
    return v;
}

inline int randint(int vmin, int vmax)
{
    return vmin + rand() % (vmax - vmin + 1);
}

template <int N, int M, typename T, typename F>
inline auto cwise_func(MatT<N,M,T> const& A, F f)
{
    using R = typename std::result_of<F(T)>::type;
    MatT<N,M,R> C;

    for (int y = 0; y < N; ++ y)
    {
        T const* srow = A.row_ptr(y);
        R* drow = C.row_ptr(y);

        for (int x = 0; x < M; ++ x)
        {
            drow[x] = f(srow[x]);
        }
    }

    return C;
}

template <int N,typename T, typename F>
inline auto cwise_func(VecT<N,T> const& a, F f)
{
    using R = typename std::result_of<F(T)>::type;
    VecT<N,R> c;

    T const* sptr = a.data();
    R* dptr = c.data();

    for (int i = 0; i < N; ++ i)
    {
        dptr[i] = f(sptr[i]);
    }

    return c;
}

template <int N, int M, typename T>
static MatT<N,M,T> cwise_abs(MatT<N,M,T> const& A)
{
    auto f = [](T val) {
        return val < 0 ? -val : val;
    };
    return cwise_func(A, f);
}

template <int N, typename T>
static VecT<N,T> cwise_abs(VecT<N,T> const& v)
{
    auto f = [](T val) {
        return val < 0 ? -val : val;
    };
    return cwise_func(v, f);
}

template <int N, typename T>
static T cwise_abs_max(VecT<N,T> const& v)
{
    T maxval = std::abs(v(0));
    for (int i = 1; i < N; ++ i)
        maxval = std::max(maxval, std::abs(v(i)));
    return maxval;
}

template <typename T>
inline T __clamp(T const& x, T const& a, T const& b)
{
    return x < a ? a :
        x > b ? b : x;
}

template <int N, typename T>
static VecT<N,T> cwise_clamp(VecT<N,T> const& v, VecT<N,T> const& vmin, VecT<N,T> const& vmax)
{
    T const* vptr = v.data();
    T const* vminptr = vmin.data();
    T const* vmaxptr = vmax.data();

    VecT<N,T> out;
    T* outptr = out.data();

    for (int i = 0; i < N; ++ i)
        outptr[i] = __clamp<T>(vptr[i], vminptr[i], vmaxptr[i]);

    return out;
}

template <int N, typename T>
static VecT<N,T> cwise_mul(VecT<N,T> const& a, VecT<N,T> const& b)
{
    VecT<N,T> c;
    T* cptr = c.data();
    T const* aptr = a.data();
    T const* bptr = b.data();

    for (int i = 0; i < N; ++ i)
        cptr[i] = aptr[i] * bptr[i];

    return c;
}

template <int N, typename T>
static VecT<N,T> cwise_div(VecT<N,T> const& a, VecT<N,T> const& b)
{
    VecT<N,T> c;
    T* cptr = c.data();
    T const* aptr = a.data();
    T const* bptr = b.data();

    for (int i = 0; i < N; ++ i)
        cptr[i] = aptr[i] / bptr[i];

    return c;
}

template <int N, int M, typename T>
static T maxval(MatT<N,M,T> const& A)
{
    T m = A(0,0);

    for (int y = 0; y < N; ++ y)
    {
        T const* srow = A.row_ptr(y);

        for (int x = 0; x < M; ++ x)
        {
            if (srow[x] > m)
                m = srow[x];
        }
    }

    return m;
}

template <int N, int M, typename T>
static VecT<N,T> mul(MatT<N,M,T> const& m, VecT<M> const& v)
{
    VecT<N,T> c;
    T const* vptr = v.data();
    T* cptr = c.data();

    for (int y = 0; y < N; ++ y)
    {
        T const* mrow = m.row_ptr(y);
        T val(0);

        for (int x = 0; x < M; ++ x)
            val += mrow[x] * vptr[x];

        cptr[y] = val;
    }

    return c;
}

template <int N, int M, int K, typename T>
static MatT<N,K,T> mul(MatT<N,M,T> const& A, MatT<M,K,T> const& B)
{
    MatT<N,K,T> C;
    C.fill(0);

    for (int i = 0; i < N; ++ i)
    {
        T const* Ai_ = A.row_ptr(i);
        T* Ci_ = C.row_ptr(i);

        for (int j = 0; j < M; ++ j)
        {
            T const* Bj_ = B.row_ptr(j);
            T Aij = Ai_[j];

            for (int k = 0; k < K; ++ k)
            {
                Ci_[k] += Aij * Bj_[k];
            }
        }
    }

    return C;
}

template <int N, int M, int K, typename T>
inline MatT<N,K,T> operator * (MatT<N,M,T> const& A, MatT<M,K,T> const& B)
{
    return mul(A,B);
}

template <int N, int M, typename T>
inline VecT<N,T> operator * (MatT<N,M,T> const& A, VecT<M> const& v)
{
    return mul(A, v);
}

template <int N, typename T>
inline VecT<N,T> operator - (VecT<N,T> const& v)
{
    VecT<N,T> c;
    T const* sptr = v.data();
    T* dptr = c.data();

    for (int i = 0; i < N; ++ i)
        dptr[i] = -sptr[i];

    return c;
}

template <int N, int M, typename T>
inline MatT<N,M,T> operator - (MatT<N,M,T> const& A)
{
    MatT<N,M,T> C;
    T const* sptr = A.data();
    T* dptr = C.data();

    for (int i = 0; i < N*M; ++ i)
        dptr[i] = -sptr[i];

    return C;
}

template <int N, int M, typename T, typename Fun>
inline bool all_satisfy(MatT<N,M,T> const& A, Fun condition)
{
    for (int y = 0; y < N; ++ y)
    {
        T const* srow = A.row_ptr(y);

        for (int x = 0; x < M; ++ x)
        {
            if (!condition(srow[x]))
                return false;
        }
    }

    return true;
}

template <int N, typename T, typename Fun>
inline bool all_satisfy(VecT<N,T> const& a, Fun condition)
{
    T const* sptr = a.data();

    for (int i = 0; i < N; ++ i)
    {
        if (!condition(sptr[i]))
            return false;
    }

    return true;
}

template <int N, int M, typename T>
static bool all_less(MatT<N,M,T> const& m, T thresh)
{
    auto f = [thresh](T elem) { return elem < thresh; };
    return all_satisfy(m, f);
}

template <int N, typename T>
static bool all_less(VecT<N,T> const& a, T thresh)
{
    auto f = [thresh](T elem) { return elem < thresh; };
    return all_satisfy(a, f);
}

template <int N, typename T>
static bool all_geq(VecT<N,T> const& a, T thresh)
{
    auto f = [thresh](T elem) { return elem >= thresh; };
    return all_satisfy(a, f);
}


template <int N, class T>
inline T dot(VecT<N,T> const& a, VecT<N,T> const& b)
{
    return a.dot(b);
}

template <int N, class T>
inline T square(VecT<N, T> const& v)
{
    return dot(v, v);
}

inline bool equal(double a, double b, double eps=1e-8)
{
    return fabs(a - b) < eps;
}

template <int N, int M, class T>
inline bool equal(MatT<N,M,T> const& A, MatT<N,M,T> const& B, T eps=1e-8)
{
    return all_less(cwise_abs(A - B), eps);
}

template <
    int N,
    typename Integer,
    std::enable_if_t<std::is_integral<Integer>::value, int> = 0
>
inline bool equal(VecT<N,Integer> const& a, VecT<N,Integer> const& b)
{
    for (int i = 0; i < N; ++ i)
    {
        if (a(i) != b(i))
            return false;
    }
    return true;
}

template <
    int N,
    typename Floating,
    std::enable_if_t<std::is_floating_point<Floating>::value, int> = 0
>
inline bool equal(VecT<N,Floating> const& a, VecT<N,Floating> const& b, Floating eps=1e-8)
{
    return all_less(cwise_abs(a - b), eps);
}

template <int N, class T>
inline MatT<N,N,T> outer(VecT<N,T> const& a, VecT<N,T> const& b)
{
    MatT<N,N,T> m;

    for (int r = 0; r < N; ++ r)
    {
        auto ar = a(r);
        for (int c = 0; c < N; ++ c)
            m(r,c) = ar * b(c);
    }

    return m;
}

template <int N, class T>
inline T norm(VecT<N,T> const& v)
{
    return v.norm();
}

template <int N, class T>
inline VecT<N,T> normalized(VecT<N,T> const& v)
{
    return v.normalized();
}

/*
 * a b' - b a'
 */
template <int N, typename T>
inline MatT<N,N,T> exterior(VecT<N,T> const& a, VecT<N,T> const& b)
{
    MatT<N,N,T> m;

    for (int r = 1; r < N; ++ r)
    {
        auto ar = a(r);
        auto br = b(r);

        for (int c = 0; c < r; ++c)
            m(r,c) = ar * b(c) - a(c) * br;
    }

    for (int r = 0; r < N; ++ r)
    {
        m(r,r) = 0.;
        for (int c = r+1; c < N; ++c)
            m(r,c) = -m(c,r);
    }

    return m;
}

template <typename T>
inline VecT<3,T> cross(VecT<3,T> const& a, VecT<3,T> const& b)
{
    return {
         a(1) * b(2) - a(2) * b(1),
        -a(0) * b(2) + a(2) * b(0),
         a(0) * b(1) - a(1) * b(0)
    };
}

constexpr int __abs(int a)
{
    return a > 0 ? a : -a;
}

constexpr int __isqrt(int a, int b, int N)
{
    return __abs(a - b) <= 1 ? a :
        __isqrt((a + N / a) / 2, N / a, N);
}

constexpr int isqrt(int N)
{
    return N <= 0 ? 0 : __isqrt(1, N, N);
}

template <int N, typename T>
inline MatT<N,N,T> perp_proj_mat(VecT<N,T> const& v)
{
    MatT<N,N,T> P = MatT<N,N,T>::eye() - outer(v,v) / dot(v,v);
    return P;
}

template <class Mat, int N>
inline void fill_mat_cols(Mat& m, std::initializer_list<VecT<N>> const& columns)
{
    int i = 0;

    for (auto col : columns)
    {
        m.set_col(i, col);
        ++ i;
    }
}

template <int N, class ... Rest>
inline auto mat_from_cols(VecT<N> const& first, Rest const& ... rest)
{
    constexpr int M = sizeof...(rest) + 1;
    MatT<N,M> m;
    fill_mat_cols(m, {first, rest...});
    return m;
}

inline Mat3x2 perp(Vec3 const& v, Vec3 const& auxiliary)
{
    // "Vectors v and auxiliary must not be collinear"
    assert(
        !equal(fabs(dot(v, auxiliary)), norm(v) * norm(auxiliary))
    );

    Vec3 x = normalized(v);
    Vec3 y = normalized(Vec3(auxiliary - x * dot(x, auxiliary)));
    Vec3 z = cross(x, y);
    Mat3x2 P;
    P.set_col(0, y);
    P.set_col(1, z);
    return P;
}

static const double __epsilon = 1e-12;

template <int M, typename T>
static bool inverse(MatT<M,M,T> const& A, MatT<M,M,T>& inv)
{
    using Mat = MatT<M,M,T>;

    Mat L = Mat::eye();
    Mat U = A;
    Mat P = Mat::eye();

    for (int n = 0; n < M; n ++)
    {
        if (fabs(U(n, n)) < __epsilon)
        {
            for (int i = n + 1; i < M; i ++)
            {
                if (fabs(U(i, n)) > 1e-8)
                {
                    U.swap_rows(i, n);
                    P.swap_rows(i, n);
                    L.swap_rows(i, n);
                    L.swap_cols(i, n);
                    break;
                }
            }
        }

        if (fabs(U(n, n)) < __epsilon)
        {
            return false;
        }

        for (int i = (n + 1); i < M; i ++)
        {
            L(i, n) = U(i, n) / U(n, n);

            for (int k = n; k < M; k++)
            {
                U(i, k) -= L(i, n) * U(n, k);
            }
        }
    }

    for (int c = 0; c < M; c++)
    {
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < i; j++)
                P(i, c) -= L(i, j) * P(j, c);
        }
    }

    for (int c = 0; c < M; c++)
    {
        for (int k = 0; k < M; k++)
        {
            int i = M - 1 - k;

            for (int j = i + 1; j < M; j++)
                P(i, c) -= U(i, j) * P(j, c);

            P(i, c) /= U(i, i);
        }
    }

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if (!std::isfinite(P(i,j)))
            {
                return false;
            }
        }
    }

    inv = P;
    return true;
}

template <int M, typename T>
static MatT<M,M,T> inverse(MatT<M,M,T> const& A)
{
    MatT<M,M,T> B;
    if (inverse(A,B))
        return B;
    B.fill(0);
    return B;
}

template <int N, class T>
inline VecT<N,T> solve(MatT<N,N,T> const& A, VecT<N,T> const& b)
{
    VecT<N,T> x;
    // TODO:
    assert(false);
    return x;
}

inline double det(Mat3 const& m)
{
    return m(0,0)*m(1,1)*m(2,2) - m(0,0)*m(1,2)*m(2,1)
        - m(0,1)*m(1,0)*m(2,2) + m(0,1)*m(1,2)*m(2,0)
        + m(0,2)*m(1,0)*m(2,1) - m(0,2)*m(1,1)*m(2,0);
}

inline Mat3 inverse(Mat3 const& m)
{
    Mat3 result {
        m(1,1)*m(2,2) - m(1,2)*m(2,1), -m(0,1)*m(2,2) + m(0,2)*m(2,1), m(0,1)*m(1,2) - m(0,2)*m(1,1),
        -m(1,0)*m(2,2) + m(1,2)*m(2,0), m(0,0)*m(2,2) - m(0,2)*m(2,0), -m(0,0)*m(1,2) + m(0,2)*m(1,0),
        m(1,0)*m(2,1) - m(1,1)*m(2,0), -m(0,0)*m(2,1) + m(0,1)*m(2,0), m(0,0)*m(1,1) - m(0,1)*m(1,0)
    };
    return result / det(m);
}

template <int N, int M, class T>
static void format_floating_mat(std::ostream& s, MatT<N,M,T> const& m)
{
    static_assert(std::is_floating_point<T>::value, "The function works only with a floating point type");

    char buf[N * M * 20];
    int c = 0;
    int n;

    n = snprintf(buf + c, sizeof(buf) - c, "[");
    assert(n > 0);
    c += n;

    for (int y = 0; y < N; ++ y)
    {
        for (int x = 0; x < M; ++ x)
        {
            if (x != M - 1)
            {
                n = snprintf(buf + c, sizeof(buf) - c, "%.6f, ", m.at(y, x));
                assert(n > 0);
                c += n;
            }
            else
            {
                n = snprintf(buf + c, sizeof(buf) - c, "%.6f", m.at(y, x));
                assert(n > 0);
                c += n;
            }
        }

        if (y != N - 1)
        {
            n = snprintf(buf + c, sizeof(buf) - c, "; ");
            assert(n > 0);
            c += n;
        }
        else
        {
            n = snprintf(buf + c, sizeof(buf) - c, "]");
            assert(n > 0);
            c += n;
        }
    }

    s << buf;
}

template <int N, int M, class T>
static void format_mat(std::ostream& s, MatT<N,M,T> const& m)
{
    s << "[";

    for (int y = 0; y < N; ++ y)
    {
        for (int x = 0; x < M; ++ x)
        {
            if (x != M - 1)
            {
                s << m.at(y, x) << ", ";
            }
            else
            {
                s << m.at(y, x);
            }
        }

        if (y != N - 1)
        {
            s << "; ";
        }
        else
        {
            s << "]";
        }
    }
}

template <int N, typename T>
static void format_floating_vec(std::ostream& s, VecT<N,T> const& v)
{
    static_assert(std::is_floating_point<T>::value, "The function works only with a floating point type");

    char buf[N * 20];
    int c = 0;
    int n;

    n = snprintf(buf + c, sizeof(buf) - c, "[");
    assert(n > 0);
    c += n;

    for (int i = 0; i < N; ++ i)
    {
        if (i != N - 1)
        {
            n = snprintf(buf + c, sizeof(buf) - c, "%.6f, ", v.at(i));
            assert(n > 0);
            c += n;
        }
        else
        {
            n = snprintf(buf + c, sizeof(buf) - c, "%.6f]", v.at(i));
            assert(n > 0);
            c += n;
        }
    }

    s << buf;
}

template <int N, typename T>
static void format_vec(std::ostream& s, VecT<N,T> const& v)
{
    s << "[";

    for (int i = 0; i < N; ++ i)
    {
        if (i != N - 1)
        {
            s << v.at(i) << ", ";
        }
        else
        {
            s << v.at(i) << "]";
        }
    }
}

template <int N, int M, typename T>
static std::ostream& operator << (std::ostream& s, MatT<N,M,T> const& m)
{
    format_mat(s, m);
    return s;
}

template <int N, int M>
static std::ostream& operator << (std::ostream& s, MatT<N,M,double> const& m)
{
    format_floating_mat(s, m);
    return s;
}

template <int N, typename T>
static std::ostream& operator << (std::ostream& s, VecT<N,T> const& v)
{
    format_vec(s, v);
    return s;
}

template <int N>
static std::ostream& operator << (std::ostream& s, VecT<N,double> const& v)
{
    format_floating_vec(s, v);
    return s;
}
