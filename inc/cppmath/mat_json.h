#pragma once

#include <cppmisc/json.h>
#include <cppmisc/throws.h>
#include "mat.h"


template <int N, class T>
static bool json_parse(Json::Value const& json, VecT<N,T>& v)
{
    int n = v.size();
    if (int(json.size()) != n)
        return false;

    for (int i = 0; i < n; ++ i)
    {
        json_parse(json[i], v(i));
    }

    return true;
}

template <int N, int M, class T>
static void json_parse(Json::Value const& json, MatT<N,M,T>& m)
{
    int const nrows = m.nrows();
    int const ncols = m.ncols();

    if (nrows != (int)json.size())
    {
        throw_runtime_error(
            "Dimension of matrix does not match: expect ", nrows, " but json has ", json.size()
        );
    }

    for (int r = 0; r < nrows; ++r)
    {
        auto row = json[r];

        if (ncols != (int)row.size())
        {
            throw_runtime_error(
                "Dimension of matrix does not match: expect ", ncols, " but json has ", row.size()
            );
        }

        for (int c = 0; c < ncols; ++c)
        {
            json_parse(row[c], m(r,c));
        }
    }
}
