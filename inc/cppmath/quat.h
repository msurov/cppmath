#pragma once

#include <ostream>
#include "mat.h"
#include "math.h"


template <typename T=double>
struct QuatT : public VecT<4,T>
{
private:
    using inharitance = VecT<4,T>;
    using Vec = VecT<3,T>;

public:
    QuatT() {}
    QuatT(T w, T x, T y, T z) : inharitance({w,x,y,z}) {}
    QuatT(T w) : QuatT(w, 0, 0, 0) {}
    QuatT(T w, Vec const& v) : QuatT(w, v(0), v(1), v(2)) {}
    QuatT(inharitance const& elems) : inharitance(elems) {}

    inline QuatT mul(QuatT const& q) const
    {
        return QuatT(
            w() * q.w() - ::dot(vec(), q.vec()),
            w() * q.vec() + vec() * q.w() + cross(vec(), q.vec())
        );
    }
    inline QuatT mul(Vec const& qv) const
    {
        return QuatT(
            -dot(vec(), qv),
            w() * qv + cross(vec(), qv)
        );
    }
    inline T w() const { return inharitance::at(0); }
    inline T x() const { return inharitance::at(1); }
    inline T y() const { return inharitance::at(2); }
    inline T z() const { return inharitance::at(3); }
    inline T& w() { return inharitance::at(0); }
    inline T& x() { return inharitance::at(1); }
    inline T& y() { return inharitance::at(2); }
    inline T& z() { return inharitance::at(3); }
    Vec vec() const { return { x(), y(), z() }; }
    inline QuatT conj() const { return QuatT(w(), -vec()); }
};

template <class T>
inline QuatT<T> operator * (QuatT<T> const& a, QuatT<T> const& b)
{
    return a.mul(b);
}

template <class T>
inline QuatT<T> operator * (QuatT<T> const& a, VecT<3,T> const& b)
{
    return a.mul(b);
}


using Quat = QuatT<>;

inline std::ostream& operator << (std::ostream& s, Quat const& q)
{
    char buf[4*33];
    sprintf(buf, "(%f; %f, %f, %f)", q.w(), q.x(), q.y(), q.z());
    s << buf;
    return s;
}

inline double quat_rot_proj(Quat const& q, Vec3 const& l_)
{
    auto l = l_.normalized();
    double w = q.w();
    Vec3 v = q.vec();
    return 2 * atan2(v.dot(l), w);
}
