#pragma once

#include <ostream>
#include "mat.h"


inline double square(double v)
{
    return v * v;
}

inline double sq(double v)
{
    return v * v;
}

inline double cross(Vec2 const& a, Vec2 const& b)
{
    return a(0) * b(1) - a(1) * b(0);
}

inline double cube(double v)
{
    return v * v * v;
}

template <typename T>
inline T sign(T const& a)
{
    return a < T(0) ? T(-1) :
        a > 0 ? T(1) : T(0);
}

template <typename T>
inline T ramp(T const& a)
{
    return a < T(0) ? T(0) : a;
}

template <typename T>
inline T clamp(T const& x, T const& a, T const& b)
{
    return x < a ? a :
        x > b ? b : x;
}
