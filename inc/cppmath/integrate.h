#pragma once
#include <vector>


namespace ode
{
    template <typename Arg, typename Val>
    struct Solution
    {
        std::vector<Arg> t;
        std::vector<Val> x;
    };

    template<typename Arg, typename Val, typename Fun>
    Solution<Arg,Val> solve(
        Fun f,
        Arg t0,
        Arg tf,
        Val const& x0,
        Arg step
    )
    {
        const int n = int(std::abs((tf - t0) / step) + 0.5);
        const double h = (tf - t0) / n;

        std::vector<Arg> t_(n + 1);
        std::vector<Val> x_(n + 1);

        Arg t = t0;
        Val x = x0;

        t_[0] = t;
        x_[0] = x;

        for (int i = 1; i <= n; ++ i)
        {
            auto k1 = f(t, x);
            auto k2 = f(t + h / 2, x + k1 * (h / 2));
            auto k3 = f(t + h / 2, x + k2 * (h / 2));
            auto k4 = f(t + h, x + k3 * h);

            x += (k1 + k2 * 2. + k3 * 2. + k4) * (h/6);
            t = t0 + (tf - t0) * i / n;

            t_[i] = t;
            x_[i] = x;
        }

        return {t_, x_};
    }
}
