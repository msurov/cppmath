#pragma once

#include "math.h"



/**
 * @brief for given val, and sorted array arr find i s.t.
 *  if val > arr[end]
 *      return end
 *  if val < arr[0]
 *      return 0
 *  return i s.t. arr[i] <= val < arr[i+1]
 */
template <typename T>
inline int __bisect(T const* arr, int sz, T const& val)
{
    if (sz <= 1)
        return 0;
    int i = sz / 2;
    if (val >= arr[i])
        return i + __bisect(arr + i, sz - i, val);
    return __bisect(arr, i, val);
}

template <typename Arr, typename Val>
inline int bisect(Arr const& arr, Val const& val)
{
    auto const& first = *arr.begin();
    auto const* parr = &first;
    const int sz = arr.size();
    return __bisect(parr, sz, val);
}


/**
 * @brief return i such that
 *  t[i] <= x < t[i+1]
 */
template <typename Knots>
inline int spline_knot_index(Knots const& t, int k, typename Knots::value_type const& x)
{
    int i = bisect(t, x);
    int n = t.size();
    i = clamp<int>(i, k, n - k - 2);
    return i;
}

/**
 * @brief De Boor's algorithm
 *
 *  t   knits
 *  c   controls
 *  k   degree of spline
 *  i   knots index x; can be found by get_knot_index(t, k, x)
 *  x   x
 *  nder order of derivative
 */
template <class CtrlsArr, class KnotsArr>
typename CtrlsArr::value_type spline_deboor(KnotsArr const& t, CtrlsArr const& c, int k,
    int i, typename KnotsArr::value_type const& x, int nder = 0)
{
    using Coef = typename KnotsArr::value_type;
    using Result = typename CtrlsArr::value_type;

    Result d[k + 1];

    for (int j = 0; j <= k; ++ j)
    {
        d[j] = c[i - k + j];
    }

    for (int der = 1; der <= nder; ++ der)
    {
        for (int j = k; j >= der; --j)
        {
            d[j] = Coef(k - der + 1) * (d[j] - d[j-1]) / (t[i+j-der+1] - t[i-k+j]);
        }
    }

    const Coef _1(1);

    for (int r = 1 + nder; r <= k; ++ r)
    {
        for (int j = k; j >= r; --j)
        {
            Coef alpha = (x - t[i-k+j]) / (t[i+j+1-r] - t[i-k+j]);
            d[j] = (_1 - alpha) * d[j-1] + alpha * d[j];
        }
    }

    return d[k];
}


/**
 * @brief Evaluate spline basis functions B_{i-k..i,k}(x)
 * or derivative of order nder of them B_{i,k}(x)
 *
 *  @param t    knots
 *  @param k    degree of spline
 *  @param x    eval basis function at point x
 *  @param i    index s.t. t[i] <= x < t[i+1]
 *  @param d    result array of coefficients of size k+1
 *  @param nder order of derivative
 */
template <typename Arg>
static void spline_eval_basis(
    Arg const* t, int k,
    int i, Arg const& x,
    Arg* d, int nder=0
    )
{
    for (int j = 0; j < k; ++ j)
        d[j] = Arg(0);
    d[k] = Arg(1);

    for (int r = 1; r < k + 1 - nder; ++ r)
    {
        {
            int j = k - r;
            auto beta = (t[i-k+1+r+j] - x) / (t[i-k+1+r+j] - t[i-k+1+j]);
            d[j] = beta * d[j+1];
        }

        for (int j = k - r + 1; j < k; ++ j)
        {
            auto alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j]);
            auto beta = (t[i+r+1-k+j] - x) / (t[i-k+r+1+j] - t[i-k+1+j]);
            d[j] = alpha * d[j] + beta * d[j+1];
        }

        {
            int j = k;
            auto alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j]);
            d[j] = alpha * d[j];
        }
    }

    for (int der = 0; der < nder; ++ der)
    {
        int p = nder - der - 1;

        {
            int j = p;
            auto beta = (k-p) / (t[i+j+1-p] - t[i-k+j+1]);
            d[j] = -beta * d[j+1];
        }

        for (int j = p + 1; j < k; ++ j)
        {
            auto alpha = (k-p) / (t[i+j-p] - t[i-k+j]);
            auto beta = (k-p) / (t[i+j+1-p] - t[i-k+j+1]);
            d[j] = alpha * d[j] - beta * d[j+1];
        }

        {
            int j = k;
            auto alpha = (k-p) / (t[i+j-p] - t[i-k+j]);
            d[j] = alpha * d[j];
        }
    }
}

/**
 * @brief evaluate value of the spline function given by the parameters
 * @param t spline knots
 * @param c spline coefficients (controls)
 * @param k spline order
 * @param x spline argument
 * @param nder order of derivative
 * @result spline value at x (or its derivative)
 */
template <typename KnotsArr, typename CtrlsArr>
inline typename CtrlsArr::value_type spline_eval_v1(
    KnotsArr const& t_, CtrlsArr const& c_, int k,
    typename KnotsArr::value_type const& x, int nder = 0)
{
    int i = spline_knot_index(t_, k, x);
    return spline_deboor(t_, c_, k, i, x, nder);
}

/**
 * @brief evaluate value of the spline function given by the parameters
 * @param t spline knots
 * @param c spline coefficients (controls)
 * @param k spline order
 * @param x spline argument
 * @param nder order of derivative
 * @result spline value at x (or its derivative)
 */
template <typename KnotsArr, typename CtrlsArr>
inline typename CtrlsArr::value_type
spline_eval(
    KnotsArr const& t_,
    CtrlsArr const& c_,
    int k,
    typename KnotsArr::value_type const& x,
    int nder = 0
    )
{
    using Result = typename CtrlsArr::value_type;
    using Coef = typename KnotsArr::value_type;

    auto const* c = &(*c_.begin());
    auto const* t = &(*t_.begin());

    int i = spline_knot_index(t_, k, x);
    Coef d[k + 1];

    spline_eval_basis(t, k, i, x, d, nder);
    Result result = d[0] * c[i - k];
    for (int j = 1; j <= k; ++ j)
        result += d[j] * c[i - k + j];

    return result;
}

template <typename ArgType, typename ValType>
class SplineT
{
private:
    const int _k;
    std::vector<ArgType> _t;
    std::vector<ValType> _c;

public:
    constexpr SplineT(std::vector<ArgType> const& knots, std::vector<ValType> const& ctrls, int k) :
        _k(k), _t(knots), _c(ctrls) {}

    inline ValType operator() (ArgType arg, int nder=0) const
    {
        return spline_eval(_t, _c, _k, arg, nder);
    }

    inline ArgType minarg() const
    {
        return *_t.begin();
    }

    inline ArgType maxarg() const
    {
        return *_t.rbegin();
    }
};

template <typename KnotsArr, typename CtrlsArr>
constexpr inline SplineT<KnotsArr, CtrlsArr> make_spline(KnotsArr const& knots, CtrlsArr const& ctrls, int k)
{
    return {knots, ctrls, k};
}
