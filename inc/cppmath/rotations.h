#pragma once
#include <ostream>
#include <math.h>
#include "quat.h"
#include "mat.h"


struct AngleAxis
{
    double angle;
    Vec3 axis;

    AngleAxis() : angle(0), axis({1,0,0}) {}

    AngleAxis(double angle, Vec3 const& axis)
    {
        double n = norm(axis);
        if (n < 1e-15)
        {
            this->angle = 0.;
            this->axis = {1.,0.,0.};
        }
        else
        {
            this->angle = angle;
            this->axis = axis / n;
        }
    }
};


template <typename T>
inline MatT<3,3,T> wedge(VecT<3,T> const& a)
{
    auto x = a(0), y = a(1), z = a(2);
    return {
         0, -z,  y,
         z,  0, -x,
        -y,  x,  0
    };
}

inline Vec3 vee(Mat3 const& A)
{
    return {A(2,1), A(0, 2), A(1, 0)};
}

/**
 * @param theta anglr
 * @param l axis
 * @result rotation matrix
 */
inline Mat3 rotmat(double theta, Vec3 const& l)
{
    double s = cos(theta / 2);
    Vec3 v = l * sin(theta / 2);
    Mat3 v_wedge = wedge(v);
    Mat3 R = Mat3::eye() * square(s) + 2 * s * v_wedge + outer(v, v) + v_wedge * v_wedge;
    return R;
}

/**
 * @param q quaternion
 * @result rotation matrix
 */
template <typename T>
inline MatT<3,3,T> rotmat(QuatT<T> const& q)
{
    QuatT<T> q_ = normalized(q);
    T
        w = q_.w(),
        x = q_.x(),
        y = q_.y(),
        z = q_.z();
    T
        x2 = square(x),
        y2 = square(y),
        z2 = square(z);

    return {
        {  1 - 2 * y2 - 2 * z2,  2 * x * y - 2 * z * w,  2 * x * z + 2 * y * w},
        {2 * x * y + 2 * z * w,    1 - 2 * x2 - 2 * z2,  2 * y * z - 2 * x * w},
        {2 * x * z - 2 * y * w,  2 * y * z + 2 * x * w,    1 - 2 * x2 - 2 * y2}
    };
}

/**
 * @param rodrigues
 * @result rotation matrix
 */
inline Mat3 rotmat(Vec3 const& rodrigues)
{
    double theta = norm(rodrigues);
    if (fabs(theta) < 1e-15)
        return Mat3::eye();
    Vec3 l = rodrigues / theta;
    return rotmat(theta, l);
}

/**
 * @param q quaternion
 * @result rodrigues
 */
inline Vec3 rodrigues(Quat const& q)
{
    if (1 - fabs(q.w()) <= 0.)
        return {0., 0., 0.};
    double theta = 2 * acos(q.w());
    return theta * q.vec() / sin(theta / 2.);
}

/**
 * @param R rotation matrix
 * @result quaternion
 */
inline Quat quat(Mat3 const& R)
{
    double t = R.trace();
    double r = sqrt(1. + t);
    double w = r / 2.;
    double x = sqrt(fabs(1 + R(0, 0) - R(1, 1) - R(2, 2))) / 2;

    if (R(2, 1) < R(1, 2))
        x = -x;
    double y = sqrt(fabs(1 - R(0, 0) + R(1, 1) - R(2, 2))) / 2;
    if (R(0, 2) < R(2, 0))
        y = -y;
    double z = sqrt(fabs(1 - R(0, 0) - R(1, 1) + R(2, 2))) / 2;
    if (R(1, 0) < R(0, 1))
        z = -z;

    return {w,x,y,z};
}

/**
 * @param aa rangleaxis
 * @result quaternion
 */
inline Quat quat(AngleAxis const& aa)
{
    Vec3 l = normalized(aa.axis);
    double theta = aa.angle;
    return Quat(cos(theta / 2.), l * sin(theta / 2.));
}

/**
 * @param q quaternion
 * @result angle-axis
 */
inline AngleAxis angleaxis(Quat const& q)
{
    if (1 - fabs(q.w()) <= 0.)
        return {0., {1., 0., 0.}};
    double theta = 2 * acos(q.w());
    Vec3 l = q.vec() / sin(theta / 2.);
    return {theta, l};
}

/**
 * @param R rotation matrix 
 * @result angle-axis
 */
inline AngleAxis angleaxis(Mat3 const& R)
{
    return angleaxis(quat(R));
}

/**
 * @param R rotation matrix 
 * @result rodrigues parameters
 */
inline Vec3 rodrigues(Mat3 const& R)
{
    return rodrigues(quat(R));
}

/**
 * @param rodrigues 
 * @result roll, pitch, yaw
 */
Vec3 rodrigues_to_rpy(Vec3 const& rodrigues);


//
// formatting
// 

inline std::ostream& operator << (std::ostream& s, AngleAxis const& aa)
{
    char buf[256];
    int n = snprintf(
        buf, sizeof(buf), "{%f, (%f, %f, %f)}", 
        aa.angle, aa.axis(0), aa.axis(1), aa.axis(2));
    if (n > 0)
        s << buf;
    return s;
}
