import numpy as np

A = np.array([
    [1, 2, 3, 4, 1],
    [3, 2, 4, 6, 5],
    [1, 2, 1, 2, 4],
    [3, 7, 5, 2, 1],
    [3, 2, 5, 6, 1]
])
print(np.linalg.inv(A))