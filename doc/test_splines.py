import numpy as np
from scipy.interpolate import make_interp_spline, BSplineT
import matplotlib.pyplot as plt
import bisect


def format_arr(arr, brleft='{', brright='}'):
    if hasattr(arr, '__len__'):
        return brleft + ', '.join([format_arr(e, brleft, brright) for e in arr]) + brright
    return '%.16f' % arr


def test1():
    t = np.linspace(0,1,100)
    t = np.exp(t)
    p = np.array([
        np.sin(2.2131*t + 3.4534),
        np.sin(1.2342*t + 1.2346),
        np.sin(6.5342*t + 8.6745)
    ]).T
    s = make_interp_spline(t, p, k=3)
    x = 1.34124
    # print('"k": %d,' % s.k)
    # print('"t": %s,' % format_arr(s.t, '[', ']'))
    # print('"c": %s,' % format_arr(s.c, '[', ']'))
    # print('"x": %.16f' % x)
    # print('"y": %s' % format_arr(s(x)))

    print('int k = %d;' % s.k)
    print('std::vector<double> t %s;' % format_arr(s.t))
    print('std::vector<Vec3> c %s;' % format_arr(s.c))
    print('double x = %.16f;' % x)
    print('Vec3 y %s;' % format_arr(s(x)))

    # print('k: %d' % s.k)
    # print('double x = %.16f;' % x)
    # print('Vec y0 {%.16f, %.16f, %.16f};' % tuple(s(x)))
    # print('Vec y0 {%.16f, %.16f, %.16f};' % tuple(s(1.1)))


def deBoor(i, x, t, c, k):
    """
    Evaluates S(x).

    Args
    ----
    i: index of knot interval that contains x
    x: position
    t: knots
    c: controls
    k: degree
    """
    d = [c[j + i - k] for j in range(0, k+1)]
    for r in range(1, k+1):
        for j in range(k, r-1, -1):
            alpha = (x - t[j+i-k]) / (t[j+1+i-r] - t[j+i-k])
            d[j] = (1.0 - alpha) * d[j-1] + alpha * d[j]
    return d[k]


def eval_spline_2(s, x):
    idx, = np.nonzero(s.t >= x)
    i = idx[0] - 1
    K = s.k
    n = len(s.t)
    i = np.clip(i, K, n - K - 2)
    y = deBoor(i, x, s.t, s.c, s.k)
    return y


def test2():
    t = np.linspace(0,1,20)
    p = np.array([
        np.sin(2.2131*t + 3.4534),
        np.sin(1.2342*t + 1.2346),
        np.sin(6.5342*t + 8.6745)
    ]).T
    s = make_interp_spline(t, p, k=5)

    x = 0.52341235231
    idx, = np.nonzero(s.t >= x)
    i = idx[0] - 1
    y = deBoor(i, x, s.t, s.c, s.k)
    print(s(x) - y)


def spline_basis(t, x, K):
    n = len(t)
    indices, = np.nonzero(t > x)
    if len(indices) == 0:
        v = n - K - 2
    else:
        v = indices[0] - 1

    v = np.clip(v, K, n - K - 2)
    B = np.zeros((K + 1, K + 1), float)
    B[0,K] = 1.

    # 
    # fill table: r -- row, c -- column
    # 
    for k in range(1, K + 1):
        
        # left
        c = K - k
        i = v + c - K
        β = (t[i+k+1] - x) / (t[i+k+1] - t[i+1])
        B[k, c] = β * B[k - 1, c + 1]

        # right
        c = K
        i = v + c - K
        α = (x - t[i]) / (t[i + k] - t[i])
        B[k, c] = α * B[k - 1, c]

        for c in range(K - k + 1, K):
            i = v + c - K
            α = (x - t[i]) / (t[i + k] - t[i])
            β = (t[i+k+1] - x) / (t[i+k+1] - t[i+1])
            B[k, c] = \
                α * B[k - 1, c] + \
                β * B[k - 1, c + 1]

    return v - K, B[K,:]


def eval_spline(s, x):
    i, b = spline_basis(s.t, x, s.k)
    return np.sum([s.c[i+j] * b[j] for j in range(0, len(b))])


def eval_spline_der(s, x):
    t = s.t
    c = s.c
    k = s.k

    def d(i):
        i = min(i, len(c) - 1)
        return k * (c[i] - c[i-1]) / (t[i+k] - t[i])

    i, b = spline_basis(t, x, k - 1)
    return np.sum([d(i + j) * b[j] for j in range(0, len(b))])


def spline_der(s):
    c = s.c
    t = s.t
    k = s.k

    def f(i):
        return k * (c[i] - c[i-1]) / (t[i+k] - t[i])

    t1 = t[1:-1]
    c1 = [f(i) for i in range(1, len(c))]
    k1 = k - 1

    return BSplineT(t=t1, c=c1, k=k1)


def test3():
    np.set_printoptions(suppress=True, linewidth=200)
    t = np.linspace(0, 1.3332, 20)
    p = np.sin(2.2131*t + 3.4534)
    s = make_interp_spline(t, p, k=5)
    x = np.linspace(t[0], t[-1], 1000)
    d1 = [s(xi) - eval_spline(s, xi) for xi in x]
    d2 = [s(xi) - eval_spline_2(s, xi) for xi in x]

    plt.plot(d1)
    plt.plot(d2)
    plt.grid()
    plt.show()


def test4():
    np.set_printoptions(suppress=True, linewidth=200)
    t = np.linspace(0, 1.3332, 20, dtype=np.float)
    p = np.sin(2.2131*t + 3.4534)
    s = make_interp_spline(t, p, k=5)
    s1 = spline_der(s)

    x = np.linspace(t[0], t[-1], 1000)
    d1 = s(x, 1) - s1(x)
    d2 = [s(xi, 1) - eval_spline_der(s, xi) for xi in x]


    plt.plot(d1)
    plt.plot(d2)
    plt.grid()
    plt.show()


def eval_spline_3(t, c, k, i, x):
    d = np.zeros((k+1,k+1), float)

    for j in range(0, k+1):
        d[0,j] = c[i - k + j]

    for r in range(1, k+1):
        for j in range(r, k+1):
            α = (x - t[i-k+j]) / (t[i+j+1-r] - t[i-k+j])
            d[r,j] = (1 - α) * d[r-1,j-1] + α * d[r-1,j]

    return d[k,k]


def eval_spline_der_3(t, c, k, i, x):
    d = np.zeros((k+1,k+1), float)

    for j in range(0, k+1):
        d[0,j] = c[i - k + j]

    for j in range(1, k+1):
        d[1,j] = k*(d[0,j] - d[0,j-1]) / (t[i+j] - t[i-k+j])

    for r in range(2, k+1):
        for j in range(r, k+1):
            α = (x - t[i-k+j]) / (t[i+j+1-r] - t[i-k+j])
            d[r,j] = (1 - α) * d[r-1,j-1] + α * d[r-1,j]

    return d[k,k]


def eval_spline_der_4(t, c, k, i, x, nder):
    d = np.zeros((k+1,k+1), float)

    for j in range(0, k+1):
        d[0,j] = c[i - k + j]

    for der in range(1, nder + 1):
        for j in range(der, k + 1):
            d[der,j] = (k - der + 1)*(d[der-1,j] - d[der-1,j-1]) / (t[i+j-der+1] - t[i-k+j])

    for r in range(1+nder, k+1):
        for j in range(r, k+1):
            α = (x - t[i-k+j]) / (t[i+j+1-r] - t[i-k+j])
            d[r,j] = (1 - α) * d[r-1,j-1] + α * d[r-1,j]

    return d[k,k]


def eval_spline_v5(t,c,k,i,x):
    d = np.zeros(k+1, float)
    d[k] = 1.

    for r in range(1, k+1):
        j = k - r
        beta = (t[i+r+1-k+j] - x) / (t[i+r+1-k+j] - t[i-k+1+j])
        d[j] = beta * d[j+1]

        for j in range(k - r + 1, k):
            alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j])
            beta = (t[i+r+1-k+j] - x) / (t[i+r+1-k+j] - t[i-k+1+j])
            d[j] = alpha * d[j] + beta * d[j+1]

        j = k
        alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j])
        d[j] = alpha * d[j]

    return np.sum([
        d[j] * c[i-k+j] for j in range(0, k+1)
    ])


def eval_spline_der_v5(t,c,k,i,x):
    d = np.zeros(k+1, float)
    d[k] = 1.

    for r in range(1, k):
        j = k - r
        beta = (t[i+r+1-k+j] - x) / (t[i+r+1-k+j] - t[i-k+1+j])
        d[j] = beta * d[j+1]

        for j in range(k - r + 1, k):
            alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j])
            beta = (t[i+r+1-k+j] - x) / (t[i+r+1-k+j] - t[i-k+1+j])
            d[j] = alpha * d[j] + beta * d[j+1]

        j = k
        alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j])
        d[j] = alpha * d[j]

    j = 0
    beta = k / (t[i+j+1] - t[i-k+j+1])
    d[j] = -beta * d[j+1]

    for j in range(1, k):
        alpha = k / (t[i+j] - t[i-k+j])
        beta = k / (t[i+j+1] - t[i-k+j+1])
        d[j] = alpha * d[j] - beta * d[j+1]

    j = k
    alpha = k / (t[i+j] - t[i-k+j])
    d[j] = alpha * d[j]

    return np.sum([
        d[j] * c[i-k+j] for j in range(0, k+1)
    ])


def eval_spline_v6(t,c,k,i,x,nder):
    d = np.zeros(k+1, float)
    d[k] = 1.

    for r in range(1, k+1-nder):
        j = k - r
        beta = (t[i+r+1-k+j] - x) / (t[i+r+1-k+j] - t[i-k+1+j])
        d[j] = beta * d[j+1]

        for j in range(k - r + 1, k):
            alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j])
            beta = (t[i+r+1-k+j] - x) / (t[i+r+1-k+j] - t[i-k+1+j])
            d[j] = alpha * d[j] + beta * d[j+1]

        j = k
        alpha = (x - t[i-k+j]) / (t[i-k+r+j] - t[i-k+j])
        d[j] = alpha * d[j]

    for der in range(0, nder):
        p = nder - der - 1
        r = k - p

        j = p
        beta = (k-p) / (t[i+j+1-p] - t[i-k+j+1])
        d[j] = -beta * d[j+1]

        for j in range(p+1, k):
            alpha = (k-p) / (t[i+j-p] - t[i-k+j])
            beta = (k-p) / (t[i+j+1-p] - t[i-k+j+1])
            d[j] = alpha * d[j] - beta * d[j+1]

        j = k
        alpha = (k-p) / (t[i+j-p] - t[i-k+j])
        d[j] = alpha * d[j]

    return np.sum([
        d[j] * c[i-k+j] for j in range(0, k+1)
    ])


def test6():
    t = np.linspace(0, 1.3332, 20)
    t = np.exp(t)
    p = np.sin(2.2131*t + 3.4534)
    s = make_interp_spline(t, p, k=5)

    x = t[5] + 0.05212;
    idx, = np.nonzero(s.t >= x)
    if len(idx) == 0:
        i = len(s.t) - s.k - 2
    else:
        i = idx[0] - 1
        i = np.clip(i, s.k, len(s.t) - s.k - 2)

    # y0 = s(x)
    # y1 = eval_spline_v5(s.t, s.c, s.k, i, x)

    # print('y0 = %.16f' % y0)
    # print('y1 = %.16f' % y1)

    # dy0 = s(x, 1)
    # dy1 = eval_spline_der_v5(s.t, s.c, s.k, i, x)

    # print('dy0 = %.16f' % dy0)
    # print('dy1 = %.16f' % dy1)

    # y0 = s(x)
    # dy0 = s(x,1)
    ddy0 = s(x,2)
    dddy0 = s(x,3)
    # y1 = eval_spline_v6(s.t, s.c, s.k, i, x, nder=0)
    # dy1 = eval_spline_v6(s.t, s.c, s.k, i, x, nder=1)
    ddy1 = eval_spline_v6(s.t, s.c, s.k, i, x, nder=2)
    dddy1 = eval_spline_v6(s.t, s.c, s.k, i, x, nder=3)

    # print('y0 = %.16f = %.16f' % (y0, y1))
    # print('dy0 = %.16f = %.16f' % (dy0, dy1))
    print('ddy0 = %.16f = %.16f' % (ddy0, ddy1))
    print('dddy0 = %.16f = %.16f' % (dddy0, dddy1))


def test5():
    np.set_printoptions(suppress=True, linewidth=200)
    t = np.linspace(0, 1.3332, 20)
    t = np.exp(t)
    p = np.sin(2.2131*t + 3.4534)
    s = make_interp_spline(t, p, k=5)

    x = t[0] + 1e-1;
    idx, = np.nonzero(s.t >= x)
    if len(idx) == 0:
        i = len(s.t) - s.k - 2
    else:
        i = idx[0] - 1
        i = np.clip(i, s.k, len(s.t) - s.k - 2)

    print('int k = %d;' % s.k)
    print('double x = %.16f;' % x)
    print('std::vector<double> t {%s};' % format_arr(s.t))
    print('std::vector<double> c {%s};' % format_arr(s.c))
    print('double y0 = %.16f;' % s(x));
    print('double dy0 = %.16f;' % s(x,1));
    print('double ddy0 = %.16f;' % s(x,2));
    print('double dddy0 = %.16f;' % s(x,3));
    print('double ddddy0 = %.16f;' % s(x,4));
    print('double dddddy0 = %.16f;' % s(x,5));

    y1 = s(x)
    y3 = eval_spline_3(s.t, s.c, s.k, i, x)

    print('Δ y1 =', y1 - y3)

    dy1 = s(x, 1)
    dy3 = eval_spline_der_3(s.t, s.c, s.k, i, x)

    print('Δ dy =', dy1 - dy3)

    dddy1 = s(x, 3)
    dddy4 = eval_spline_der_4(s.t, s.c, s.k, i, x, nder=3)

    print('Δ dddy =', dddy1 - dddy4)

    ddy1 = s(x, 2)
    ddy4 = eval_spline_der_4(s.t, s.c, s.k, i, x, nder=2)

    print('Δ ddy =', ddy1 - ddy4)


if __name__ == '__main__':
    test1()
    # test6()
